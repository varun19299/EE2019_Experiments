Duty Measurements


Vctrl         Duty

2.0, 6.5
2.1, 14.9
2.2, 28.1
2.3, 40.6
2.4, 45.6
2.5, 52.8
2.6, 60.1
2.7, 72.5
2.8, 80
2.9, 87.3
3.0, 95.5
3.1, 97.9

Frequency at 50% duty: 97.32 kHz

Values used:

C1= 1nF
R1= 12K
R2= 1.5 K
R3= 7.5k
Pull ups= 910
C_bias=1nF
R_bias = 100K
