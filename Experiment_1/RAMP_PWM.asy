Version 4
SymbolType BLOCK
RECTANGLE Normal -96 -56 96 56
WINDOW 0 0 -56 Bottom 2
WINDOW 3 0 56 Top 2
SYMATTR Value RAMP_PWM
SYMATTR Prefix X
SYMATTR ModelFile /Users/Ankivarun/Documents/Academics/IIT M/SEM 4/E_EE2019/Lab/Experiment_1/RAMP_PWM.net
PIN -96 -16 LEFT 8
PINATTR PinName Vdd
PINATTR SpiceOrder 1
PIN -96 16 LEFT 8
PINATTR PinName Vcm
PINATTR SpiceOrder 2
PIN -96 48 LEFT 8
PINATTR PinName Vbias
PINATTR SpiceOrder 3
PIN -96 -48 LEFT 8
PINATTR PinName Vctrl
PINATTR SpiceOrder 4
PIN 96 -16 RIGHT 8
PINATTR PinName Vpwm
PINATTR SpiceOrder 5
PIN 96 16 RIGHT 8
PINATTR PinName Vramp
PINATTR SpiceOrder 6
