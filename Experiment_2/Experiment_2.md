# Answers to Experiment 2

Hardware Section

## Question 3

C_n, C_p =330 pF

Non-overlap time = 500ns

## Question 5

Time period: 10us
Frq: 100kHz
Pos Duty: 50.3%
Negative : 49.7%

Lag in rise and fall

## Question 6 (of V_out)

220mV ripple amplitude
2.67 V average VALUE

Freq: 100kHz

## Question 7

V_in = 5V
Duty: 0, V_out=0V

Duty: 25, V_out=1.26V;
230mV ripple amplitude
Freq: 100kHz

Duty: 50, V_out=2.67V
30mV ripple amplitude
Freq: 100kHz

Duty: 75, V_out=3.87V
220mV ripple amplitude
Freq: 100kHz

Duty: 100, V_out
220mV ripple amplitude
Freq: 100kHz

## Question 8
