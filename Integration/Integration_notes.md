## List

Dated: 14th April, 2018.

1. [x] Fix up Vref (change 2kHz to off)
2. [x] Speaker sinking current over complimentary terminal
3. [x] Noise fine-tuning.
4. [ ] Separate all sources.

5. [ ]Measure all output waveforms, correct loading distortions.

## Waveform Measurements

* V_peak: Recovers at 470 mV for 1K, 390 for 3K

* V_ramp: 1.64V (peak to peak), 87kHz, 2.59 V mean

* V_ramp_class_D: 1.64V (peak to peak), 87kHz, 3.08 V mean.

* V_ramp_B: Mean at 2.6V

Slight effect of loading the speaker at 1,3 kHz on the ramp waveform.

To document:

* Speaker characteristics
